/*
 * Задание 1
 * 
 * Напишите функцию, которая возвращает нечетные значения массива.
 * [1,2,3,4] => [1,3]
 */
export const getOddValues = arr => {
  let nonEvenNumbers = [];
  arr.forEach(function(item, i, arr) {
      if(item % 2 !== 0) nonEvenNumbers.push(arr[i]);
    });
  return nonEvenNumbers;
}


/*
 * Задание 2
 * 
 * Напишите функцию, которая возвращает наименьшее значение массива
 * [1,2,3,4] => 1
 */
export const getMinValue = arr => {
  return Math.min.apply( null, arr );
}


/*
 * Задание 3
 * 
 * Напишите функцию, которая возвращает массив наименьших значение строк двумерного массива
 * [
 *   [1,2,3,4],
 *   [1,2,3,4],
 *   [1,2,3,4],
 *   [1,2,3,4],
 * ] 
 * => [1,1,1,1]
 *
 * Подсказка: вложенные for
 */
export const getMinValuesFromRows = arr => {
  let minElDlArray = [];
  for ( let i = 0; i < arr.length; i++ ){
          minElDlArray.push(Math.min.apply(null,arr[i])); 
  };
  return minElDlArray;
}


/*
 * Задание 4
 * 
 * Напишите функцию, которая возвращает 2 наименьших значение массива
 * [4,3,2,1] => [1,2]
 *
 * Подсказка: sort
 */
export const get2MinValues = arr => {
    const b = arr.sort(function(a, b) {
      if (a > b) return 1;
      if (a == b) return 0;
      if (a < b) return -1;
     });       
    return b.slice(0,2);
};


/*
 * Задание 5
 * 
 * Напишите функцию, которая возвращает количество гласных в строке
 * ( a, e, i, o, u ).
 * 
 * 'Return the number (count) of vowels in the given string.' => 15
 * 
 * Подсказка: indexOf/includes или (reduce, indexOf/includes) или (filter, indexOf/includes)
 */
export const getCountOfVowels = str => {
  let count = 0;
  for (let char of str) {
    if (char === 'a' || char === 'e' || char === 'i' || char === 'o' || char === 'u') count++ 
  }
  return count;
};


 /*
  * Задание 6
  * 
  * Реализовать функцию, на входе которой массив чисел, на выходе массив уникальных значений
  * [1, 2, 2, 4, 5, 5] => [1, 2, 4, 5]
  * 
  * Подсказка: reduce
  */
 export const getUniqueValues = str => {
  return str.reduce(function (acc, currentValues) {
    return acc.indexOf( currentValues) < 0 ? [...acc,  currentValues] : acc 
  }, []);

};


 /*
  * Задание 7
  * 
  * Реализовать функцию, на входе которой массив строк, на выходе массив с длинами этих строк
  *  ['Есть', 'жизнь', 'на', 'Марсе'] => [4, 5, 2, 5]
  * 
  * Подсказка: map
  */
export const getStringLengths = arr => {
  return arr.map(v => v.length);
};


 /*
  * Задание 8
  * 
  * Напишите функцию, которая принимает на вход данные из корзины в следующем виде:
  *  [
  *      { price: 10, count: 2},
  *      { price: 100, count: 1},
  *      { price: 2, count: 5},
  *      { price: 15, count: 6},
  *  ]
  * где price это цена товара, а count количество. Функция должна вернуть 
  * итоговую сумму по данному заказу.
  */
export const getTotal = cartData => {
  return cartData.reduce((acc, { price, count }) => acc + price * count, 0);
};


 /*
  * Задание 9
  * 
  * Реализовать функцию, на входе которой число с ошибкой, на выходе строка с сообщением
  * 500 => Ошибка сервера
  * 401 => Ошибка авторизации
  * 402 => Ошибка сервера
  * 403 => Доступ запрещен
  * 404 => Не найдено
  * ХХХ => '' (для остальных - пустая строка)
  */
export const getError = errorCode => {
  switch(errorCode){
    case 500: return 'Ошибка сервера'; break;
    case 401: return 'Ошибка авторизации'; break;
    case 402: return 'Ошибка сервера'; break;
    case 403: return 'Доступ запрещен'; break;
    case 404: return 'Не найдено'; break;
    default:  return '';
  };
};


 /*
  * Задание 10
  * 
  * Реализовать функцию, на входе которой объект следующего вида:
  * {
  *   firstName: 'Петр',
  *   secondName: 'Васильев',
  *   patronymic: 'Иванович'
  * }
  * на выходе строка с сообщением 'ФИО: Петр Иванович Васильев'
  */
export const getFullName = user => {
  return `ФИО: ${user.firstName} ${user.patronymic} ${user.secondName}`;
};


 /*
  * Задание 11
  * 
  * Реализовать функцию, которая принимает на вход 2 аргумента: массив чисел и множитель,
  * а возвращает массив исходный массив, каждый элемент которого был умножен на множитель:
  * 
  * [1,2,3,4], 5 => [5,10,15,20]
  */
export const getNewArray = (arr, mul) => {
  return arr.map(v => v*mul);
};


 /*
  * Задание 12
  * 
  * Реализовать функцию, которая принимает на вход 2 аргумента: массив и франшизу,
  * а возвращает строку с именнами героев разделенных запятой:
  * 
  * [
  *    {name: “Batman”, franchise: “DC”},
  *    {name: “Ironman”, franchise: “Marvel”},
  *    {name: “Thor”, franchise: “Marvel”},
  *    {name: “Superman”, franchise: “DC”}
  * ],
  * Marvel
  * => Ironman, Thor
  */

 export const getHeroes = (heroes, franchise) => {
  const filterHeroes = heroes.filter(
      heroes => heroes.franchise === franchise
  );
  const arrName = filterHeroes.map(
      function(filterName){
        return filterName.name;
      }
  );
  const result = arrName.join(', ');
  return result;
};

